/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DepartmentDetail_departments = ReadonlyArray<{
    readonly id: string;
    readonly " $fragmentRefs": FragmentRefs<"DepartmentInfo_department">;
    readonly " $refType": "DepartmentDetail_departments";
}>;
export type DepartmentDetail_departments$data = DepartmentDetail_departments;
export type DepartmentDetail_departments$key = ReadonlyArray<{
    readonly " $data"?: DepartmentDetail_departments$data;
    readonly " $fragmentRefs": FragmentRefs<"DepartmentDetail_departments">;
}>;



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "DepartmentDetail_departments",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "DepartmentInfo_department"
    }
  ],
  "type": "Department",
  "abstractKey": null
};
(node as any).hash = 'bf81db488d846382875ca7d7cafb6b62';
export default node;
