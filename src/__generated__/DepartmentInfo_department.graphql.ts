/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DepartmentInfo_department = {
    readonly id: string;
    readonly " $refType": "DepartmentInfo_department";
};
export type DepartmentInfo_department$data = DepartmentInfo_department;
export type DepartmentInfo_department$key = {
    readonly " $data"?: DepartmentInfo_department$data;
    readonly " $fragmentRefs": FragmentRefs<"DepartmentInfo_department">;
};



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "DepartmentInfo_department",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Department",
  "abstractKey": null
};
(node as any).hash = 'a591d9cd4af1bd65b0be5b4477721a5e';
export default node;
