import { useRelayEnvironment } from 'react-relay/hooks'
import { useAlert } from '@saastack/core'
import { FormContainerProps } from '@saastack/layouts/types'
import { useNavigate } from '@saastack/router'
import React from 'react'
import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'
import CreateDepartmentMutation from '../mutations/CreateDepartmentMutation'
import DepartmentAddFormComponent from '../forms/DepartmentAddFormComponent'
import { Variables } from 'relay-runtime'
import { PubSub } from '@saastack/pubsub'
import { Trans } from '@lingui/macro'
import namespace from '../namespace'
import { FormContainer } from '@saastack/layouts/containers'
import DepartmentAddInitialValues from '../utils/DepartmentAddInitialValues'
import DepartmentAddValidations from '../utils/DepartmentAddValidations'

interface Props extends Omit<FormContainerProps, 'formId'> {
    variables: Variables
}

const formId = 'Department-add'

const DepartmentAdd: React.FC<Props> = ({ variables, ...props }) => {
    const environment = useRelayEnvironment()
    const showAlert = useAlert()
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(true)
    const [loading, setLoading] = React.useState(false)

    const navigateBack = () => navigate('../')
    const handleClose = () => setOpen(false)

    const onSuccess = (response: any) => {
        PubSub.publish(namespace.create, response)
        setLoading(false)
        if (props.onClose) {
            props.onClose()
        }
        handleClose()

        showAlert(<Trans>Department added successfully!</Trans>, {
            variant: 'info',
        })
    }

    const onError = (e: string) => {
        showAlert(e, {
            variant: 'error',
        })
        setLoading(false)
    }

    const handleSubmit = (values: DepartmentInput) => {
        setLoading(true)
        const departmentInput: DepartmentInput = {
            ...values,
        }
        CreateDepartmentMutation.commit(environment, variables, departmentInput, {
            onSuccess,
            onError,
        })
    }

    const initialValues = {
        ...DepartmentAddInitialValues,
    }

    return (
        <FormContainer
            formId={formId}
            header={<Trans>New Department</Trans>}
            loading={loading}
            onClose={handleClose}
            open={open}
            onExited={navigateBack}
        >
            <DepartmentAddFormComponent
                initialValues={initialValues}
                validationSchema={DepartmentAddValidations}
                id={formId}
                onSubmit={handleSubmit}
            />
        </FormContainer>
    )
}

export default DepartmentAdd
