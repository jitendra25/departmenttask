import { useRelayEnvironment } from 'react-relay/hooks'
import { Trans } from '@lingui/macro'
import { useAlert } from '@saastack/core'
import { ConfirmContainer } from '@saastack/layouts/containers'
import { PubSub } from '@saastack/pubsub'
import namespace from '../namespace'
import { useNavigate, useParams } from '@saastack/router'
import DeleteDepartmentMutation from '../mutations/DeleteDepartmentMutation'
import React from 'react'
import { Variables } from 'relay-runtime'

interface Props {
    variables: Variables
}

const DepartmentDelete: React.FC<Props> = ({ variables }) => {
    const showAlert = useAlert()
    const environment = useRelayEnvironment()
    const { id } = useParams()
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(true)
    const [loading, setLoading] = React.useState(false)

    const handleDelete = () => {
        setLoading(true)
        DeleteDepartmentMutation.commit(environment, variables, window.atob(id!), {
            onSuccess,
            onError,
        })
    }

    const navigateBack = () => navigate('../../')
    const handleClose = () => setOpen(false)

    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }

    const onSuccess = (id: string) => {
        PubSub.publish(namespace.delete, id)
        setLoading(false)
        showAlert(<Trans>Department deleted successfully!</Trans>, {
            variant: 'info',
        })
        handleClose()
    }

    return (
        <ConfirmContainer
            loading={loading}
            header={<Trans>Delete Department</Trans>}
            open={open}
            onClose={handleClose}
            onExited={navigateBack}
            onAction={handleDelete}
        />
    )
}

export default DepartmentDelete
