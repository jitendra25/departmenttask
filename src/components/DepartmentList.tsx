import { ListContainer } from '@saastack/layouts/containers'
import { ListContainerProps } from '@saastack/layouts/types'
import { Trans } from '@lingui/macro'
import { ListItem, ListItemAvatar, ListItemText } from '@material-ui/core'
import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { DepartmentList_departments } from '../__generated__/DepartmentList_departments.graphql'
import Avatar from '@saastack/components/Avatar'
interface Props extends Omit<ListContainerProps<DepartmentList_departments>, 'items'> {
    departments: DepartmentList_departments
}

const DepartmentList: React.FC<Props> = ({ departments, onItemClick, ...props }) => {
    const [visibleColumns, onVisibleColumnsChange] = React.useState(['id'])
    const listProps = {
        render: (e: DepartmentList_departments[0]) => {
            return [
                <ListItemAvatar >
                    <Avatar  title={e.name} />
                </ListItemAvatar>,
                <ListItemText key="id" primary={e.name}  />,
            ]
        },
    }
   
    return (
        <ListContainer<DepartmentList_departments>
            items={departments}
            listProps={listProps}
            onItemClick={onItemClick}
            {...props}
        />
    )
}

export default createFragmentContainer(DepartmentList, {
    departments: graphql`
        fragment DepartmentList_departments on Department @relay(plural: true) {
            id
            name
            description
        }
    `,
})
