import React from 'react'
import { FormProps } from '@saastack/forms/types'
import { Form, Input,Textarea } from '@saastack/forms'
import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'
import { Toggle } from '@saastack/components'

export interface Props extends FormProps<DepartmentInput> {}

const DepartmentAddFormComponent: React.FC<Props> = (props) => {
    const [show , setShow] = React.useState(false);
    return (<Form {...props} >
        <Input placeholder={"name"} variant={"standard"} name={"name"} label={"name"} large autoFocus />
        <Toggle label={"add description"} show={show} onShow={setShow}>
            <Textarea  placeholder={"description"} label={"description"} name={"description"}/>
        </Toggle>
    </Form>)
}

export default DepartmentAddFormComponent
