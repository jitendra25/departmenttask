import React from 'react'
import hooks from '@saastack/hooks'
import { t } from '@lingui/macro'
import loadable from '@loadable/component'
import { Roles } from '@saastack/core/roles'
import { AppsOutlined } from '@material-ui/icons'

const DepartmentPage = loadable(() => import('./pages/DepartmentPage'))

let loaded = false

export function load() {
    if (loaded) {
        return
    }

    hooks.settings.items.registerHook('resourceTypes', {
        component: DepartmentPage,
        icon: AppsOutlined,
        path: '/Departments',
        title: t`Departments`,
        role: [Roles.DepartmentsAdmin, Roles.DepartmentsEditor, Roles.DepartmentsViewer],
        level: 'com',
        appName: 'Departments',
    })

    loaded = true
}

load()
