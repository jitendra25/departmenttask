import { commitMutation, graphql } from 'react-relay'
import { MutationCallbacks, setNodeValue } from '@saastack/relay'
import { Disposable, Environment, RecordSourceSelectorProxy } from 'relay-runtime'
import {
    DepartmentInput,
    UpdateDepartmentInput,
    UpdateDepartmentMutation,
    UpdateDepartmentMutationResponse,
} from '../__generated__/UpdateDepartmentMutation.graphql'
import { pick } from 'lodash'

const mutation = graphql`
    mutation UpdateDepartmentMutation($input: UpdateDepartmentInput) {
        updateDepartment(input: $input) {
            clientMutationId
            payload {
                id
                name 
                description
            }
        }
    }
`

let tempID = 0

const commit = (
    environment: Environment,
    department: DepartmentInput,
    updateMask: string[],
    callbacks?: MutationCallbacks<DepartmentInput>
): Disposable => {
    const input: UpdateDepartmentInput = {
        updateMask: { paths: updateMask },
        department,
        clientMutationId: `${tempID++}`,
    }
    return commitMutation<UpdateDepartmentMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        optimisticUpdater: (store: RecordSourceSelectorProxy) => {
         const temp = store.get(department.id!)
         if(temp){
          setNodeValue(store,temp,pick(department,updateMask))
         }
        },
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: (response: UpdateDepartmentMutationResponse) => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess({ ...department, ...response.updateDepartment.payload })
            }
        },
    })
}

export default { commit }
