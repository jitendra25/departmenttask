const namespace = {
    fetch: 'department/fetch',
    create: 'department/create',
    update: 'department/update',
    delete: 'department/delete',
}

export default namespace
