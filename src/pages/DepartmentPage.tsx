import { ErrorComponent, Loading } from '@saastack/components'
import { useConfig } from '@saastack/core'
import { LayoutProps } from '@saastack/layouts'
import { useQuery } from '@saastack/relay'
import { useDidMountEffect } from '@saastack/utils'
import React from 'react'
import { graphql } from 'react-relay'
import { DepartmentPageQuery } from '../__generated__/DepartmentPageQuery.graphql'
import DepartmentMaster from '../components/DepartmentMaster'

const query = graphql`
    query DepartmentPageQuery($parent: String) {
        ...DepartmentMaster_departments @arguments(parent: $parent)
    }
`

interface PageProps {
    parent?: string
    layoutProps?: LayoutProps
}

const DepartmentPage: React.FC<PageProps> = (props) => {
    const { companyId } = useConfig()
    const parent = (props.parent || companyId)!

    const variables: DepartmentPageQuery['variables'] = {
        parent,
    }
    const { data, loading, error, refetch } = useQuery<DepartmentPageQuery>(query, variables)
    useDidMountEffect(() => {
        refetch(variables)
    }, [parent])
    if (loading) {
        return <Loading />
    }
    if (error) {
        return <ErrorComponent error={error} />
    }

    return <DepartmentMaster {...props} departments={data!} parent={parent} />
}

export default DepartmentPage
