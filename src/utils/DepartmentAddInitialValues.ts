import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'

const DepartmentAddInitialValues: DepartmentInput = {
    name:'',
    description:''
}

export default DepartmentAddInitialValues
