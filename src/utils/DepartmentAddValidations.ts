import * as yup from 'yup'
import { t } from '@lingui/macro'

const DepartmentAddValidations = yup.object().shape({
    "name":yup.string().required("required" as string).max(100,"max 100 char allowed"),
    "description":yup.string().max(2000,"max 2000 char allowed" as string)
})

export default DepartmentAddValidations
